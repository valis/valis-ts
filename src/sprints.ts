import * as yaml from 'js-yaml';

export enum TaskStatus {
    Todo = 'todo',
    InProgress = 'inprogress',
    InReview = 'inreview',
    Backlog = 'backlog',
    Done = 'done'
}

export enum TaskPriority {
    Top = 'top',
    High = 'high',
    Normal = 'normal',
    Low = 'low'
}

export interface MarkdownTask {
    name: string;
    status: TaskStatus;
    startDate?: string;
    dueDate?: string;
    scheduledDate?: string;
    priority?: TaskPriority;
}

export interface MarkdownSprintMetadata {
    start: Date;
    end: Date;
    name: string;
    type: string;
    project: string;
    uri: string;
}

export interface MarkdownSprint {
    metadata: MarkdownSprintMetadata;
    content: string;
}


export interface MarkdownSprintTaskCounts {
    todo: MarkdownTask[];
    backlog: MarkdownTask[];
    inProgress: MarkdownTask[];
    inReview: MarkdownTask[];
    done: MarkdownTask[];
}


/**
 * Get the task status from the text
 * @param text {@link string}
 * @returns A {@link TaskStatus}
 */
export function getTaskStatus(text: string): TaskStatus {
    if (text.startsWith('- [ ]')) { return TaskStatus.Todo; }
    if (text.startsWith('- [b]')) { return TaskStatus.Backlog; }
    if (text.startsWith('- [p]')) { return TaskStatus.InProgress; }
    if (text.startsWith('- [r]')) { return TaskStatus.InReview; }
    if (text.startsWith('- [x]')) { return TaskStatus.Done; }
    return TaskStatus.Todo;
}

/**
 *  Get the date from the text
 * @param text 
 * @param emoji 
 * @returns 
 */
export function getDateFromText(text: string, emoji: string): string | undefined {
    const dateMatch = text.match(new RegExp(`${emoji} (\\d{4}-\\d{2}-\\d{2})`));
    return dateMatch ? dateMatch[1] : undefined;
}

/**
 * Get the priority from the text
 */
export function getPriorityFromText(text: string): TaskPriority {
    if (text.includes('🔼')) { return TaskPriority.High; }
    if (text.includes('⏫')) { return TaskPriority.Top; }
    if (text.includes('🔽')) { return TaskPriority.Low; }
    return TaskPriority.Normal;
}

/**
 * Get the task name from the text
 * @param text 
 * @returns 
 */
export function getTaskName(text: string): string {
    // Remove anything inside brackets
    let name = text.replace(/\[.*?\]/g, '').trim();
    // Remove status emojis and dates
    name = name.replace(/(🛫|⏳|📅|🔼|⏫|🔽) (\d{4}-\d{2}-\d{2})?/g, '').trim();
    return name;
}
interface Frontmatter {
    Name?: string;
    Type: string;
    Start?: string;
    End: string;
}

/**
 * Get the task name from the text
 * @param markdown 
 * @param newName 
 * @returns 
 */
export function extractTasks(markdown: string, newName: string): {original: string, extracted: string} {
    const frontmatterEndIndex = markdown.indexOf('---', 3);
    const originalFrontmatterYaml = markdown.slice(3, frontmatterEndIndex);
    const originalFrontmatter = yaml.load(originalFrontmatterYaml) as Frontmatter;
    const originalEnd = new Date(originalFrontmatter.End);

    const newStart = new Date(originalEnd.getTime() + 24 * 60 * 60 * 1000); // next day
    const newEnd = new Date(newStart.getTime() + 21 * 24 * 60 * 60 * 1000); // 21 days later

    const newFrontmatter = {
        Name: newName,
        Type: originalFrontmatter.Type,
        Start: newStart.toISOString().split('T')[0],
        End: newEnd.toISOString().split('T')[0],
    };

    const newFrontmatterYaml = '---\n' + yaml.dump(newFrontmatter) + '---';

    let original = [markdown.slice(0, frontmatterEndIndex + 3)];
    let extracted = [newFrontmatterYaml];

    const lines = markdown.slice(frontmatterEndIndex + 4).split('\n');

    let currentHeader: string | null = null;
    let lastHeader: string | null = null;
    let lastOriginalHeader: string | null = null;
    let currentTaskDone: boolean | null = null;

    for (let line of lines) {
        const isHeader = line.startsWith('# ');
        const isTask = line.startsWith('- [');
        const isSubTask = line.startsWith('  - [');
        const taskStatus = getTaskStatus(line.trim());

        if (isHeader) { // Only consider lines that start with '# ' as headers
            currentHeader = line;
            currentTaskDone = null;
        } else if (isTask) {
            currentTaskDone = taskStatus === TaskStatus.Done;
        }

        if (currentTaskDone === null || currentTaskDone === true) {
            if (currentHeader && currentHeader !== lastOriginalHeader) {
                original.push(currentHeader);
                lastOriginalHeader = currentHeader;
            }
            original.push(line);
        } else {
            if (currentHeader && currentHeader !== lastHeader) {
                extracted.push(currentHeader);
                lastHeader = currentHeader;
            }
            extracted.push(line);
        }
    }

    return {original: original.join('\n'), extracted: extracted.join('\n')};
}
